require "jekyll"
require "jekyll-polylist/version"
require 'faraday'
require 'json'

class PolyListEmbed < Liquid::Tag

  def initialize(tagName, content, tokens)
    super
    @assets = []
    @content = content
    @apikey = Jekyll.configuration({})['google_poly']['API_key']
  end

  def generate_iframe(poly_id)
    %Q{<style>.embed-container {  position: relative;  padding-bottom: 56.25%;  height: 0;  overflow: hidden;  max-width: 100%;}.embed-container iframe,.embed-container object,.embed-container embed {  position: absolute;  top: 0;  left: 0;  width: 100%;  height: 100%;}</style><div class=".embed-container"><iframe width="100%" height="480px" src="https://poly.google.com/view/#{ poly_id }/embed" frameborder="0" style="border:none;" allowvr="yes" allow="vr; xr; accelerometer; magnetometer; gyroscope; autoplay;" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" onmousewheel="" ></iframe></div>}
  end

  def generate_from_template(context, tmpl_path, poly_id)
    @poly_id = poly_id
    tmpl = File.read tmpl_path
    site = context.registers[:site]
    tmpl = (Liquid::Template.parse tmpl).render site.site_payload.merge!({"poly_id" => @poly_id})
  end

  def generate(context, poly_id)
    tmpl_path = File.join Dir.pwd, "_includes", "poly.html"
    if File.exist? tmpl_path
      generate_from_template context, tmpl_path, poly_id
    else
      generate_iframe poly_id
    end
  end

  def get_assets(nextPageToken = "")
    unless nextPageToken.nil?
      pageToken = "&pageToken=#{ nextPageToken }"
    else
      pageToken = ""
    end
    url = "https://poly.googleapis.com/v1/assets?key=#{ @apikey }&keywords=#{ @keyword }&pageSize=100" + pageToken
    response = Faraday.new(url).get
    if response.status == 200
      body = JSON.parse(response.body)
      body["assets"].each do |asset|
        @assets << asset
      end
      unless body["nextPageToken"].nil?
        get_assets(nextPageToken = body["nextPageToken"])
      end
    end
  end

  def render(context)
    @keyword = "#{ context[@content.strip] }"
    get_assets()
    output = "<div>"
    @assets.each do |asset|
      if asset["name"][/assets\/([^\?]*)/]
        poly_id = $1
        iframe = generate(context, poly_id)
        output += "<div class='poly-asset-container'>"
        output += "<h3 class='poly-asset-title'>#{ asset['displayName'] }</h3>"
        output += "<div class='poly-asset-iframe'>"
        output += iframe
        output += "</div>"
        output += "</div>"
      end
    end
    output += "</div>"
    return output
  end

  Liquid::Template.register_tag "google_poly_list", self
end
