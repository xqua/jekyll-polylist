# jekyll-poly

This Jekyll plugin provides a liquid tag that needs a Google Poly URL as input and will generates a (responsive) html snippet to embed the video into your site.

## To Do
Add tests to this gem.

## Requirements
You will need to have a **Google Poly API key** created and have activated the Google Poly API on your google account.

Visit the https://developers.google.com/poly/develop/api and activate your Google Poly API (Not the Oauth section !) following the button Get API Key.

You will need to add this API key to your _config.yml file:

```
google_poly:
  API_key: YOUR_API_KEY
```

## Installation

Add this line to your Gemfile:

```ruby
group :jekyll_plugins do
  gem "jekyll-polylist"
end
```

And then execute:

    $ bundle

Alternatively install the gem yourself as:

    $ gem install jekyll-polylist

and put this in your ``_config.yml``

```yaml
plugins: [jekyll-polylist]
 # This will require each of these gems automatically.
```

## Usage

### To get the Poly assets you created and made public
```
 {% google_poly_list %}
```

### To get Poly assets that follow a keyword query

```
{% google_poly_list "keyword" %}
```

## Result

By default the plugin will output the following code


```markup
<style>
.google-poly-embed-container {
  position: relative;
  padding-bottom: 56.25%;
  height: 0;
  overflow: hidden;
  max-width: 100%;
}
.google-poly-embed-container iframe,
.google-poly-embed-container object,
.google-poly-embed-container embed {
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
}
</style>
<ul>
  <li>
    <div class='google-poly-embed-container'>
      <iframe width="100%" height="480px" src="https://poly.google.com/view/7aCY_MMZWm8/embed" frameborder="0" style="border:none;" allowvr="yes" allow="vr; xr; accelerometer; magnetometer; gyroscope; autoplay;" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" onmousewheel="" ></iframe>
    </div>
  </li>
  <li>
  ....
</ul>
```

You can specify your own snippet by creating a partial ``_includes/poly.html``. Inside that partial the Google Poly ID is available as ``{{ poly_id }}``.
